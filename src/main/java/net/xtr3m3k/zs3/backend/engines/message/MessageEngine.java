package net.xtr3m3k.zs3.backend.engines.message;

import net.xtr3m3k.zs3.backend.connectors.database.MongoConnector;
import net.xtr3m3k.zs3.backend.objects.message.Message;
import net.xtr3m3k.zs3.backend.objects.message.MessagePriority;
import org.bson.Document;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MessageEngine {
    protected static List<Message> cache = new ArrayList<>();
    protected static List<Message> active = new ArrayList<>();

    public static void start() {
        if (!MongoConnector.collectionExists("messages")) MongoConnector.getDatabase().createCollection("messages");
        for (Document document : MongoConnector.getDatabase().getCollection("messages").find()) {
            Message message = deserialize(document);
            cache.add(message);
            if (!message.isActive()) {
                message.setExpired(true);
                message.synchronize();
                continue;
            }
            active.add(message);
        }

        System.out.println("Loaded " + cache.size() + " messages");
        System.out.println("     + " + active.size() + " active messages");
    }

    public static Message search(long created) {
        for (Message message : cache) {
            if (message.getCreated() == created)
                return message;
        }
        return null;
    }

    private static Message deserialize(Document document) {
        Message post = new Message();

        post.setTitle(document.getString("title"));
        post.setAuthor(document.getString("author"));
        post.setCreated(document.getLong("created"));
        post.setExpires(document.getLong("expires"));
        post.setPriority(MessagePriority.valueOf(document.getString("priority")));
        post.setText(document.getString("text"));

        return post;
    }

    public static List<Message> getActiveMessages() {
        List<Message> expired = new ArrayList<>();
        active.stream().filter(message -> !message.isActive()).forEach(message -> {
            message.setExpired(true);
            expired.add(message);
            message.synchronize();
        });
        active.removeAll(expired);
        return active;
    }

    public static void add(Message message) {
        cache.add(message);
        active.add(message);
        message.insert();
    }

    public static void remove(Message message) {
        cache.remove(message);
        active.remove(message);
        message.remove();
    }

    public static void main(String[] args) {
        System.out.println(new Date().toInstant().plusSeconds(600).toEpochMilli());
    }

    public static List<Message> getMessages() {
        return cache;
    }
}
