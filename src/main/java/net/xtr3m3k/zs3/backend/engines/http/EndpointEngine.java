package net.xtr3m3k.zs3.backend.engines.http;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import net.xtr3m3k.zs3.backend.objects.endpoint.Endpoint;
import net.xtr3m3k.zs3.backend.objects.endpoint.EndpointExecutor;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.HashMap;

public class EndpointEngine implements HttpHandler {
    private static HashMap<String, HttpHandler> endpoints = new HashMap<>();

    public static void registerEndpoint(Object clazz) {
        Endpoint annotation = clazz.getClass().getDeclaredAnnotation(Endpoint.class);
        for (Method method : clazz.getClass().getDeclaredMethods()) {
            if (method.getDeclaredAnnotation(EndpointExecutor.class) == null) continue;
        }
    }

    public void handle(HttpExchange httpExchange) throws IOException {
        String[] split = httpExchange.getRequestURI().toString().split("/");
        endpoints.get("/" + split[1]).handle(httpExchange);
    }
}
