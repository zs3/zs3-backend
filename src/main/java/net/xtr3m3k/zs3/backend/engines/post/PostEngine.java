package net.xtr3m3k.zs3.backend.engines.post;

import net.xtr3m3k.zs3.backend.connectors.database.MongoConnector;
import net.xtr3m3k.zs3.backend.objects.post.Post;
import org.bson.Document;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;

public class PostEngine {
    protected static HashMap<Long, Post> cache = new HashMap<>();

    public static void start() {
        if (!MongoConnector.collectionExists("posts")) MongoConnector.getDatabase().createCollection("posts");
        for (Document document : MongoConnector.getDatabase().getCollection("posts").find()) {
            Post post = deserialize(document);
            cache.put(post.getCreated(), post);
        }

        System.out.println("Loaded " + cache.size() + " posts");
    }

    private static Post deserialize(Document document) {
        Post post = new Post();

        post.setText(document.getString("text"));
        post.setCreated(document.getLong("created"));
        post.setAuthor(document.getString("author"));
        post.setHeading(document.getString("heading"));
        post.setPhotos((List<String>) document.get("photos"));

        return post;
    }

    public static void add(Post post) {
        cache.put(post.getCreated(), post);
        post.insert();
    }

    public static void remove(Post post) {
        cache.remove(post.getCreated(), post);
        post.remove();
    }

    public static Collection<Post> getPosts() {
        return cache.values();
    }
}
