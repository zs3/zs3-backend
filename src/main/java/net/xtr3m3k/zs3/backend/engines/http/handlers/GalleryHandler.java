package net.xtr3m3k.zs3.backend.engines.http.handlers;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import net.xtr3m3k.zs3.backend.connectors.database.MongoConnector;
import net.xtr3m3k.zs3.backend.engines.gallery.GalleryEngine;
import net.xtr3m3k.zs3.backend.engines.user.UserEngine;
import net.xtr3m3k.zs3.backend.objects.gallery.Event;
import net.xtr3m3k.zs3.backend.objects.user.User;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class GalleryHandler implements HttpHandler {
    private static List<String> convert(String uid, int max) {
        List<String> list = new ArrayList<>();
        for (int i = 1; i <= max; i++) {
            list.add("/uploads/" + uid + "_" + i + ".jpg");
        }
        return list;
    }

    public static void addRequest(NewEventRequest request) {
        User user = UserEngine.searchToken(request.token);
        if (user == null) {
            return;
        }
        int i = PhotoUploadHandler.imageMap.get(request.uid);
        Event event = new Event();
        event.setName(request.name);
        event.setYear(request.year.substring(0, 4) + "/" + request.year.substring(5, request.year.length()));
        event.setPhotos(convert(request.uid, i));
        GalleryEngine.add(event);
    }

    public void handle(HttpExchange httpExchange) throws IOException {
        Headers headers = httpExchange.getResponseHeaders();
        headers.add("Access-Control-Allow-Origin", "*");

        switch (httpExchange.getRequestMethod()) {
            case "GET":
                byte[] bytes = MongoConnector.getGson().toJson(GalleryEngine.getEvents()).getBytes(StandardCharsets.UTF_8);
                httpExchange.sendResponseHeaders(200, bytes.length);
                httpExchange.getResponseBody().write(bytes);
                httpExchange.getResponseBody().flush();
                httpExchange.getResponseBody().close();
                break;
            case "OPTIONS":
                headers.add("Access-Control-Allow-Methods", "GET,PUT,OPTIONS");
                headers.add("Access-Control-Allow-Headers", "Content-Type, Content-Length, X-Requested-With");
                httpExchange.sendResponseHeaders(200, 0);
                httpExchange.close();
                break;
            case "PUT":
                String json = IOUtils.toString(httpExchange.getRequestBody());
                byte[] bytes1 = "OK".getBytes(StandardCharsets.UTF_8);
                httpExchange.sendResponseHeaders(200, bytes1.length);
                httpExchange.getResponseBody().write(bytes1);
                httpExchange.getResponseBody().close();
                addRequest(MongoConnector.getGson().fromJson(json, NewEventRequest.class));
                break;
        }
    }

    class NewEventRequest {
        private String uid;
        private String name;
        private String year;
        private String token;
    }
}
