package net.xtr3m3k.zs3.backend.engines.http.handlers;

import com.google.gson.JsonObject;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import net.xtr3m3k.zs3.backend.Main;
import net.xtr3m3k.zs3.backend.connectors.database.MongoConnector;
import net.xtr3m3k.zs3.backend.engines.http.HttpInitializer;
import net.xtr3m3k.zs3.backend.engines.post.PostEngine;
import net.xtr3m3k.zs3.backend.engines.user.UserEngine;
import net.xtr3m3k.zs3.backend.objects.post.Post;
import net.xtr3m3k.zs3.backend.objects.user.User;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class PostsHandler implements HttpHandler {
    private static List<String> convert(String uid, int max) {
        List<String> list = new ArrayList<>();
        for (int i = 1; i <= max; i++) {
            list.add("/uploads/" + uid + "_" + i + ".jpg");
        }
        return list;
    }

    public void handle(HttpExchange httpExchange) throws IOException {
        Headers headers = httpExchange.getResponseHeaders();
        headers.add("Access-Control-Allow-Origin", "*");

        switch (httpExchange.getRequestMethod()) {
            case "GET":
                byte[] bytes = MongoConnector.getGson().toJson(PostEngine.getPosts()).getBytes(StandardCharsets.UTF_8);
                httpExchange.sendResponseHeaders(200, bytes.length);
                httpExchange.getResponseBody().write(bytes);
                httpExchange.getResponseBody().flush();
                httpExchange.getResponseBody().close();
                break;
            case "OPTIONS":
                headers.add("Access-Control-Allow-Methods", "GET,PUT,OPTIONS");
                headers.add("Access-Control-Allow-Headers", "Content-Type, Content-Length, X-Requested-With");
                httpExchange.sendResponseHeaders(200, 0);
                httpExchange.close();
                break;
            case "PUT":
                JsonObject object = Main.jsonParser.parse(IOUtils.toString(httpExchange.getRequestBody())).getAsJsonObject();
                if (!object.has("token")) {
                    httpExchange.sendResponseHeaders(400, 0L);
                    return;
                }
                User user = UserEngine.searchToken(object.get("token").getAsString());
                if (user == null) {
                    HttpInitializer.sendError(httpExchange, "0x00");
                    return;
                }
                int i = PhotoUploadHandler.imageMap.get(object.get("uid").getAsString());
                Post post = new Post();
                post.setAuthor(user.getFullName());
                post.setCreated(System.currentTimeMillis());
                post.setHeading(object.get("title").getAsString());
                post.setText(object.get("text").getAsString());
                post.setPhotos(convert(object.get("uid").getAsString(), i));
                PostEngine.add(post);
                httpExchange.sendResponseHeaders(200, 0);
                httpExchange.close();
                break;
            case "DELETE":

                break;
        }
    }
}
