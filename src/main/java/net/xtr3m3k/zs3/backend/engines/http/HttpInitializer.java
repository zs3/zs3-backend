package net.xtr3m3k.zs3.backend.engines.http;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;
import net.xtr3m3k.zs3.backend.engines.http.handlers.*;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class HttpInitializer {
    public static Gson gson = new Gson();
    private static ExecutorService internalExecutorService = Executors.newSingleThreadExecutor();

    public static void start() throws IOException {
        HttpServer httpServer = HttpServer.create(new InetSocketAddress("0.0.0.0", 8008), 1);
        httpServer.setExecutor(null);
        httpServer.createContext("/auth", new AuthHandler());
        httpServer.createContext("/users", new UsersHandler());
        httpServer.createContext("/messages", new MessagesHandler());
        httpServer.createContext("/posts", new PostsHandler());
        httpServer.createContext("/upload", new PhotoUploadHandler());
        httpServer.createContext("/files", new FilesHandler());
        httpServer.createContext("/gallery", new GalleryHandler());
        httpServer.start();
    }

    public static void sendError(HttpExchange httpExchange, String errorCode) throws IOException {
        JsonObject json = new JsonObject();
        json.addProperty("error", errorCode);
        byte[] bytes = json.toString().getBytes(StandardCharsets.UTF_8);
        httpExchange.sendResponseHeaders(200, bytes.length);
        httpExchange.getResponseBody().write(bytes);
        httpExchange.getResponseBody().flush();
        httpExchange.getResponseBody().close();
    }
}
