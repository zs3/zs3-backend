package net.xtr3m3k.zs3.backend.engines.http.handlers;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import net.xtr3m3k.zs3.backend.connectors.database.MongoConnector;
import net.xtr3m3k.zs3.backend.engines.http.HttpInitializer;
import net.xtr3m3k.zs3.backend.engines.message.MessageEngine;
import net.xtr3m3k.zs3.backend.engines.user.UserEngine;
import net.xtr3m3k.zs3.backend.objects.message.Message;
import net.xtr3m3k.zs3.backend.objects.message.MessagePriority;
import net.xtr3m3k.zs3.backend.objects.user.User;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class MessagesHandler implements HttpHandler {
    public void handle(HttpExchange httpExchange) throws IOException {
        Headers headers = httpExchange.getResponseHeaders();
        headers.add("Access-Control-Allow-Origin", "*");
        headers.add("Content-Type", "application/json");

        switch (httpExchange.getRequestMethod()) {
            case "OPTIONS":
                headers.add("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS");
                headers.add("Access-Control-Allow-Headers", "Content-Type, Authorization, Content-Length, X-Requested-With");
                httpExchange.sendResponseHeaders(200, 0);
                httpExchange.close();
                break;
            case "GET":
                byte[] bytes = MongoConnector.getGson().toJson(MessageEngine.getActiveMessages()).getBytes(StandardCharsets.UTF_8);
                httpExchange.sendResponseHeaders(200, bytes.length);
                httpExchange.getResponseBody().write(bytes);
                httpExchange.getResponseBody().flush();
                httpExchange.getResponseBody().close();
                break;
            case "POST":
                bytes = MongoConnector.getGson().toJson(MessageEngine.getMessages()).getBytes(StandardCharsets.UTF_8);
                httpExchange.sendResponseHeaders(200, bytes.length);
                httpExchange.getResponseBody().write(bytes);
                httpExchange.getResponseBody().flush();
                httpExchange.getResponseBody().close();
                break;
            case "PUT":
                JsonObject request = new JsonParser().parse(IOUtils.toString(httpExchange.getRequestBody())).getAsJsonObject();
                User token = UserEngine.searchToken(request.get("token").getAsString());
                if (token == null) {
                    HttpInitializer.sendError(httpExchange, "0x00");
                    return;
                }
                Message message = new Message();

                message.setTitle(request.get("data").getAsJsonObject().get("title").getAsString());
                message.setText(request.get("data").getAsJsonObject().get("text").getAsString());
                message.setPriority(MessagePriority.valueOf(request.get("data").getAsJsonObject().get("priority").getAsString()));
                message.setAuthor(token.getFullName());
                message.setExpires(request.get("data").getAsJsonObject().get("expires").getAsNumber().longValue());

                MessageEngine.add(message);
                httpExchange.sendResponseHeaders(200, 0L);
                break;
            case "DELETE":
                String[] split = httpExchange.getRequestURI().toString().split("/");
                if (split.length != 3) {
                    httpExchange.sendResponseHeaders(400, 0L);
                    return;
                }
                MessageEngine.remove(MessageEngine.search(Long.valueOf(split[2])));
                httpExchange.sendResponseHeaders(200, 0L);
                httpExchange.close();
                break;
        }
    }
}
