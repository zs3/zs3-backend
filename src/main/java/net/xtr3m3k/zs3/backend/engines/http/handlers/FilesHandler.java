package net.xtr3m3k.zs3.backend.engines.http.handlers;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FilesHandler implements HttpHandler {
    public void handle(HttpExchange httpExchange) throws IOException {
        Headers headers = httpExchange.getResponseHeaders();
        headers.add("Access-Control-Allow-Origin", "*");
        headers.add("Content-Type", "application/json");

        switch (httpExchange.getRequestMethod()) {
            case "OPTIONS":
                headers.add("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS");
                headers.add("Access-Control-Allow-Headers", "Content-Type, Authorization, Content-Length, X-Requested-With");
                httpExchange.sendResponseHeaders(200, 0);
                httpExchange.close();
                break;
            case "GET":
                byte[] bytes = Files.readAllBytes(Paths.get("files.json"));
                httpExchange.sendResponseHeaders(200, bytes.length);
                httpExchange.getResponseBody().write(bytes);
                httpExchange.getResponseBody().flush();
                httpExchange.getResponseBody().close();
                break;
        }
    }
}
