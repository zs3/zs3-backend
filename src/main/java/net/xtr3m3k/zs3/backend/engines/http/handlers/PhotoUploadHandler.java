package net.xtr3m3k.zs3.backend.engines.http.handlers;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import gnu.trove.map.hash.TObjectIntHashMap;
import org.apache.commons.io.IOUtils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.Base64;

public class PhotoUploadHandler implements HttpHandler {
    public static TObjectIntHashMap<String> imageMap = new TObjectIntHashMap<>();

    private static int[] getScaledSize(BufferedImage img) {
        boolean landscape = img.getWidth() > img.getHeight();
        float aspect = 100.0F;
        DecimalFormat format = new DecimalFormat("#.##");
        if (landscape) {
            if (img.getWidth() > 2560) {
                aspect = Float.valueOf(format.format(2560.0 / img.getWidth()).replace(",", "."));
            }
        } else {
            if (img.getHeight() > 2560) {
                aspect = Float.valueOf(format.format(2560.0 / img.getHeight()).replace(",", "."));
            }
        }
        return new int[]{Math.round(img.getWidth() * aspect), Math.round(img.getHeight() * aspect)};
    }

    private static void scale(BufferedImage img, String fileCore) throws IOException {
        int[] scaledSize = getScaledSize(img);
        BufferedImage output = new BufferedImage(scaledSize[0], scaledSize[1], img.getType());
        output.getGraphics().drawImage(img, 0, 0, scaledSize[0], scaledSize[1], null);
        output.getGraphics().dispose();
        ImageIO.write(output, "jpg", Paths.get("uploads/" + fileCore + ".jpg").toFile());
    }

    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        Headers headers = httpExchange.getResponseHeaders();
        headers.add("Access-Control-Allow-Origin", "*");
        headers.add("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS");
        headers.add("Access-Control-Allow-Headers", "Content-Type, Authorization, Content-Length, X-Requested-With, Parent-Post");

        switch (httpExchange.getRequestMethod()) {
            case "OPTIONS":
                headers.add("Access-Control-Allow-Methods", "POST,OPTIONS");
                headers.add("Access-Control-Allow-Headers", "Content-Type, Authorization, Content-Length, X-Requested-With");
                httpExchange.sendResponseHeaders(200, 0);
                httpExchange.close();
                break;
            case "POST":
                if (httpExchange.getRequestHeaders().get("Parent-Post") == null) {
                    httpExchange.sendResponseHeaders(403, 0);
                    httpExchange.close();
                    return;
                }
                String key = httpExchange.getRequestHeaders().get("Parent-Post").get(0);
                imageMap.adjustOrPutValue(key, 1, 1);
                String fileCore = key + "_" + imageMap.get(key);
                String s = new String(IOUtils.toByteArray(httpExchange.getRequestBody()), StandardCharsets.UTF_8);
                Files.write(Paths.get("/vhosts/content.zs3.xtr3m3k.net/uploads/" + fileCore + ".jpg"),
                        Base64.getDecoder().decode(s.substring(s.indexOf(",") + 1, s.length())));
                httpExchange.sendResponseHeaders(200, 0);
                httpExchange.close();
        }
    }
}
