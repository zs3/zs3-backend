package net.xtr3m3k.zs3.backend.engines.user;

import net.xtr3m3k.zs3.backend.connectors.database.MongoConnector;
import net.xtr3m3k.zs3.backend.objects.user.User;
import net.xtr3m3k.zs3.backend.utils.EncryptionUtil;
import org.bson.Document;

import java.util.HashMap;

public class UserEngine {
    protected static HashMap<String, User> cache = new HashMap<>();

    public static void start() {
        if (!MongoConnector.collectionExists("users")) MongoConnector.getDatabase().createCollection("users");
        for (Document document : MongoConnector.getDatabase().getCollection("users").find()) {
            User user = deserialize(document);
            cache.put(user.getEmail(), user);
        }
        System.out.println("Loaded " + cache.size() + " users");
    }

    private static User deserialize(Document document) {
        User user = new User();

        user.setEmail(document.getString("email"));
        user.setFullName(document.getString("fullName"));
        user.setPassword(document.getString("password"));
        user.setToken(document.getString("token"));
        user.setUsername(document.getString("username"));

        return user;
    }

    public static void hash(User user) {
        user.setToken(EncryptionUtil.hash(user.getUsername() + ":" + user.getPassword()));
        user.setPassword(EncryptionUtil.hash(user.getPassword()));
    }

    public static void add(User user) {
        user.setToken(EncryptionUtil.hash(user.getUsername() + ":" + user.getPassword()));
        user.setPassword(EncryptionUtil.hash(user.getPassword()));
        user.insert();
    }

    public static User search(String email) {
        return cache.get(email);
    }

    public static User searchToken(String token) {
        for (User user : cache.values()) {
            if (user.getToken().equals(token))
                return user;
        }
        return null;
    }
}
