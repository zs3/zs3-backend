package net.xtr3m3k.zs3.backend.engines.http.handlers;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import net.xtr3m3k.zs3.backend.connectors.database.MongoConnector;
import net.xtr3m3k.zs3.backend.engines.http.HttpInitializer;
import net.xtr3m3k.zs3.backend.engines.user.UserEngine;
import net.xtr3m3k.zs3.backend.objects.user.User;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class AuthHandler implements HttpHandler {
    public void handle(HttpExchange httpExchange) throws IOException {
        Headers headers = httpExchange.getResponseHeaders();
        headers.add("Access-Control-Allow-Origin", "*");
        headers.add("Content-Type", "application/json");

        switch (httpExchange.getRequestMethod()) {
            case "OPTIONS":
                headers.add("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS");
                headers.add("Access-Control-Allow-Headers", "Content-Type, Authorization, Content-Length, X-Requested-With");
                httpExchange.sendResponseHeaders(200, 0);
                httpExchange.close();
                return;
            case "POST":
                AuthenticationRequest request = MongoConnector.getGson().fromJson(IOUtils.toString(httpExchange.getRequestBody()), AuthenticationRequest.class);
                if (request == null) {
                    httpExchange.sendResponseHeaders(400, 0L);
                    return;
                }
                User search = UserEngine.search(request.email);
                if (search == null) {
                    HttpInitializer.sendError(httpExchange, "0x00");
                    return;
                }
                if (!search.getToken().equals(request.token)) {
                    HttpInitializer.sendError(httpExchange, "0x01");
                    return;
                }
                byte[] bytes = MongoConnector.getGson().toJson(search).getBytes(StandardCharsets.UTF_8);
                httpExchange.sendResponseHeaders(200, bytes.length);
                httpExchange.getResponseBody().write(bytes);
                httpExchange.getResponseBody().flush();
                httpExchange.getResponseBody().close();
                return;
            default:
                httpExchange.sendResponseHeaders(405, 0L);
                return;
        }
    }

    class AuthenticationRequest {
        private String email;
        private String token;
    }
}
