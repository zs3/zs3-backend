package net.xtr3m3k.zs3.backend.engines.http.handlers;

import com.google.gson.JsonObject;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import net.xtr3m3k.zs3.backend.connectors.database.MongoConnector;
import net.xtr3m3k.zs3.backend.engines.http.HttpInitializer;
import net.xtr3m3k.zs3.backend.engines.user.UserEngine;
import net.xtr3m3k.zs3.backend.objects.user.User;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class UsersHandler implements HttpHandler {
    public void handle(HttpExchange httpExchange) throws IOException {
        Headers headers = httpExchange.getResponseHeaders();
        headers.add("Access-Control-Allow-Origin", "*");
        headers.add("Access-Control-Allow-Methods", "GET,POST,OPTIONS");

        switch (httpExchange.getRequestMethod()) {
            case "GET":
                String[] split = httpExchange.getRequestURI().toString().split("/");
                if (split.length != 3) {
                    httpExchange.sendResponseHeaders(400, 0L);
                    return;
                }
                User search = UserEngine.search(split[2]);
                if (search == null) {
                    HttpInitializer.sendError(httpExchange, "0x00");
                    return;
                }
                JsonObject json = new JsonObject();
                json.addProperty("username", search.getUsername());
                byte[] bytes = json.toString().getBytes(StandardCharsets.UTF_8);
                httpExchange.sendResponseHeaders(200, bytes.length);
                httpExchange.getResponseBody().write(bytes);
                httpExchange.getResponseBody().flush();
                httpExchange.getResponseBody().close();
                break;
            case "POST":
                User user = MongoConnector.getGson().fromJson(IOUtils.toString(httpExchange.getRequestBody()), User.class);
                UserEngine.add(user);
                httpExchange.sendResponseHeaders(200, 0L);
                break;
            default:
                httpExchange.sendResponseHeaders(405, 0L);
                break;
        }
    }
}
