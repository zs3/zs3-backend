package net.xtr3m3k.zs3.backend.engines.gallery;

import net.xtr3m3k.zs3.backend.connectors.database.MongoConnector;
import net.xtr3m3k.zs3.backend.objects.gallery.Event;
import org.bson.Document;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class GalleryEngine {
    protected static HashMap<String, List<Event>> cache = new HashMap<>();

    public static void start() {
        if (!MongoConnector.collectionExists("events")) MongoConnector.getDatabase().createCollection("events");
        for (Document document : MongoConnector.getDatabase().getCollection("events").find()) {
            Event event = deserialize(document);
            if (!cache.containsKey(event.getYear())) cache.put(event.getYear(), new ArrayList<>());
            cache.get(event.getYear()).add(event);
        }

        System.out.println("Loaded " + cache.size() + " events");
    }

    private static Event deserialize(Document document) {
        Event event = new Event();

        event.setYear(document.getString("year"));
        event.setName(document.getString("name"));
        event.setPhotos((List<String>) document.get("photos"));

        return event;
    }

    public static void add(Event event) {
        if (!cache.containsKey(event.getYear()))
            cache.put(event.getYear(), new ArrayList<>());
        cache.get(event.getYear()).add(event);
        event.insert();
    }

    public static HashMap<String, List<Event>> getEvents() {
        return cache;
    }
}
