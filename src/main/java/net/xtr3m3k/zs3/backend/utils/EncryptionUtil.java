package net.xtr3m3k.zs3.backend.utils;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class EncryptionUtil {
    public static boolean compare(String hash, String password) {
        return hash(password).equals(hash);
    }

    public static String hash(String string) {
        MessageDigest localMessageDigest = forName("SHA1");
        assert localMessageDigest != null;
        localMessageDigest.reset();
        localMessageDigest.update(string.getBytes());
        byte[] arrayOfByte = localMessageDigest.digest();
        return String.format("%0" + (arrayOfByte.length << 1) + "x", new BigInteger(1, arrayOfByte));
    }

    protected static MessageDigest forName(String digest) {
        try {
            return MessageDigest.getInstance(digest);
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
    }
}
