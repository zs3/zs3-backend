package net.xtr3m3k.zs3.backend.objects.endpoint;

public enum Method {
    ALL, GET, POST, PUT, DELETE
}
