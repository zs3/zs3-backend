package net.xtr3m3k.zs3.backend.objects.endpoint;

public @interface Endpoint {
    String path() default "/";

    Method method() default Method.ALL;
}
