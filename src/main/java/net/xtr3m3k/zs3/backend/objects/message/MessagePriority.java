package net.xtr3m3k.zs3.backend.objects.message;

public enum MessagePriority {
    INFO, SUCCESS, DANGER, WARNING
}
