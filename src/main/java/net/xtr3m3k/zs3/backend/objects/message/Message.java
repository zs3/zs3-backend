package net.xtr3m3k.zs3.backend.objects.message;

import net.xtr3m3k.zs3.backend.connectors.database.MongoConnector;
import org.bson.Document;

public class Message {
    private String title;
    private String text;
    private long created = System.currentTimeMillis();
    private long expires;
    private String author;
    private MessagePriority priority;
    private boolean expired = false;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public long getExpires() {
        return expires;
    }

    public void setExpires(long expires) {
        this.expires = expires;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public MessagePriority getPriority() {
        return priority;
    }

    public void setPriority(MessagePriority priority) {
        this.priority = priority;
    }

    public boolean isExpired() {
        return expired;
    }

    public void setExpired(boolean expired) {
        this.expired = expired;
    }

    public boolean isActive() {
        return !(expired || expires < System.currentTimeMillis());
    }

    public void remove() {
        MongoConnector.getDatabase().getCollection("messages").deleteOne(new Document("created", created));
    }

    public void insert() {
        MongoConnector.getDatabase().getCollection("messages").insertOne(Document.parse(MongoConnector.getGson().toJson(this)));
    }

    public void synchronize() {
        MongoConnector.getDatabase().getCollection("messages").updateOne(MongoConnector.getDatabase().getCollection("messages").find(new Document("created", created)).first(), new Document("$set", Document.parse(MongoConnector.getGson().toJson(this))));
    }
}
