package net.xtr3m3k.zs3.backend.objects.user;

import net.xtr3m3k.zs3.backend.connectors.database.MongoConnector;
import org.bson.Document;

public class User {
    private String username;
    private String email;
    private String password;
    private String token;
    private String fullName;

    public static void main(String[] args) {
        User user = new User();

        user.setUsername("XTR3M3K");
        user.setFullName("Mateusz Kucz");
        user.setEmail("mati.kucz65@gmail.com");
        user.setPassword("Komputer12");
        user.setToken("TO_FILL");

        System.out.println(MongoConnector.getGson().toJson(user));
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void remove() {
        MongoConnector.getDatabase().getCollection("users").deleteOne(new Document("email", email));
    }

    public void insert() {
        MongoConnector.getDatabase().getCollection("users").insertOne(Document.parse(MongoConnector.getGson().toJson(this)));
    }

    public void synchronize() {
        MongoConnector.getDatabase().getCollection("users").updateOne(MongoConnector.getDatabase().getCollection("users").find(new Document("email", email)).first(), new Document("$set", Document.parse(MongoConnector.getGson().toJson(this))));
    }
}
