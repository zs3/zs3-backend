package net.xtr3m3k.zs3.backend.objects.gallery;

import net.xtr3m3k.zs3.backend.connectors.database.MongoConnector;
import org.bson.Document;

import java.util.HashMap;
import java.util.List;

public class Event {
    private String year;
    private String name;
    private List<String> photos;

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getPhotos() {
        return photos;
    }

    public void setPhotos(List<String> photos) {
        this.photos = photos;
    }

    public void remove() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("name", name);
        map.put("year", year);
        MongoConnector.getDatabase().getCollection("events").deleteOne(new Document(map));
    }

    public void insert() {
        MongoConnector.getDatabase().getCollection("events").insertOne(Document.parse(MongoConnector.getGson().toJson(this)));
    }

    public void synchronize() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("name", name);
        map.put("year", year);
        MongoConnector.getDatabase().getCollection("events").updateOne(MongoConnector.getDatabase().getCollection("events").find(new Document(map)).first(), new Document("$set", Document.parse(MongoConnector.getGson().toJson(this))));
    }
}
