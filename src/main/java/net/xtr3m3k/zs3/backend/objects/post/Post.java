package net.xtr3m3k.zs3.backend.objects.post;

import net.xtr3m3k.zs3.backend.connectors.database.MongoConnector;
import org.bson.Document;

import java.util.List;

public class Post {
    private String heading;
    private String text;
    private long created = System.currentTimeMillis();
    private List<String> photos;
    private String author = "Mateusz Kucz";

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public List<String> getPhotos() {
        return photos;
    }

    public void setPhotos(List<String> photos) {
        this.photos = photos;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void remove() {
        MongoConnector.getDatabase().getCollection("posts").deleteOne(new Document("heading", heading));
    }

    public void insert() {
        MongoConnector.getDatabase().getCollection("posts").insertOne(Document.parse(MongoConnector.getGson().toJson(this)));
    }

    public void synchronize() {
        MongoConnector.getDatabase().getCollection("posts").updateOne(MongoConnector.getDatabase().getCollection("posts").find(new Document("created", created)).first(), new Document("$set", Document.parse(MongoConnector.getGson().toJson(this))));
    }
}
