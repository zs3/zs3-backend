package net.xtr3m3k.zs3.backend;

import com.google.gson.JsonParser;
import net.xtr3m3k.zs3.backend.connectors.database.MongoConnector;
import net.xtr3m3k.zs3.backend.engines.gallery.GalleryEngine;
import net.xtr3m3k.zs3.backend.engines.http.HttpInitializer;
import net.xtr3m3k.zs3.backend.engines.message.MessageEngine;
import net.xtr3m3k.zs3.backend.engines.post.PostEngine;
import net.xtr3m3k.zs3.backend.engines.user.UserEngine;

import java.io.IOException;

public class Main {
    public static JsonParser jsonParser = new JsonParser();

    public static void main(String[] args) {
        init();
    }

    public static void init() {
        MongoConnector.start();
        UserEngine.start();
        PostEngine.start();
        MessageEngine.start();
        GalleryEngine.start();
        try {
            HttpInitializer.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
