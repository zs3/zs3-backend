package net.xtr3m3k.zs3.backend.connectors.database;


import com.google.gson.Gson;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;

public class MongoConnector {
    private static MongoClient client;
    private static MongoDatabase zs3;
    private static Gson gson = new Gson();

    public static void start() {
        client = new MongoClient("localhost", 27017);
        zs3 = client.getDatabase("zs3");
    }

    public static MongoClient getClient() {
        return client;
    }

    public static MongoDatabase getDatabase() {
        return zs3;
    }

    public static Gson getGson() {
        return gson;
    }

    public static boolean collectionExists(String collection) {
        for (String s : zs3.listCollectionNames()) {
            if (s.equals(collection)) {
                return true;
            }
        }
        return false;
    }
}
